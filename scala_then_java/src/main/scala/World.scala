import scala.reflect.BeanProperty

case class World(i:Int, @BeanProperty greeting:String) {
}
// vim: set ts=4 sw=4 et:
