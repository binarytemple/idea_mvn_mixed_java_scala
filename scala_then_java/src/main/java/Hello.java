
public class Hello {

    public String greeting;

    public Hello(World w) {
        this.greeting = w.getGreeting();
    }

    @Override
    public String toString() {
        return "Hello{" +
                "greeting='" + greeting + '\'' +
                '}';
    }
}
