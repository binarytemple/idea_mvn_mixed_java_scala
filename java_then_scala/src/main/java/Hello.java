
public class Hello {

    private String greeting;

    public Hello(String greeting) {
        this.greeting = greeting;
    }

    @Override
    public String toString() {
        return "Hello{" +
                "greeting='" + greeting + '\'' +
                '}';
    }
}
